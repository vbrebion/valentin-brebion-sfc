# Vue SFC - Centre de Recyclage de l'Agglomération de Bordeaux

Bienvenue dans cette évaluation de vos compétences avec le framework Vue.

## Consignes

Cette application de suivi des Centres de Recyclage de l'Agglomération Bordelaise a été faite en un seul fichier, ce qui va compromettre son évolution et la collaboration de toute l'équipe dans le futur !

Sauvez votre équipe d'une dette technique monstrueuse en la réécrivant avec les standards du framework et ajoutez des fonctionnalités pour vos utilisateurs.

- Découpez au moins en 2 composants
- Créez une page détaillée d'un centre
- Créez 1 service
- Affichez la bonne couleur avec le bon statut du Centre : VERT = ouvert, ROUGE = fermé.
- Affichez les déchets refusés dans chaque Centre
- Améliorer la qualité code du projet en général
- Permettez d'afficher une liste de 3, 6 ou 9 centres.

👉 Créez un dépôt sur GitLab nommé: "[Votre Nom] - SFC"

## Deadline

**Vous avez 1h30 pour faire cette refacto** de l'application avant que votre chef de projet lance le prochain sprint avec toute l'équipe, faites de votre mieux !

Vous travaillez **seul** sur ce projet.

## Ressources

Votre équipe n'a pas encore scrapper toutes les informations, vous vous reposez donc sur l'Open-Data API de la ville.
[Documentation de l'API](https://opendata.bordeaux-metropole.fr/explore/dataset/dechetteries-en-temps-reel/information/)

Vous disposez d'une première mise en forme grâce à Tailwindcss et DaisyUI.
Si nécessaire, Pinia et VueRouter sont d'ors-et-déjà dans installer dans le projet.
