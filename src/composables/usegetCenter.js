import { ref } from 'vue';
import apiService from '@/services/api.service';


export default function useGetCenter() {
    const apiError = ref('');
    const centers = ref()
    const totalCenters = ref()

    async function getCenters(params) {
        try {
            const response = await apiService.getCenters(params);
            totalCenters.value = response.data.nhits
            centers.value = response.data.records.map((record) => {
              return {
                ...record.fields,
                acceptes: record.fields.acceptes.split(';')
              }
            })
            console.log(response);
        } catch(error) {
            if (error.response && error.response.data && error.response.data.error) {
                apiError.value = error.response.data.error.message;
            } else {
                apiError.value = "Une erreur s'est produite lors du traitement de votre demande.";
            }
        }
    }
    return {
        getCenters,
        totalCenters,
        centers,
        apiError
    };
}