import { createRouter, createWebHistory } from 'vue-router'

//views
const HomeView = () => import('../views/HomeView.vue')

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
  }
  ]
})

export default router
